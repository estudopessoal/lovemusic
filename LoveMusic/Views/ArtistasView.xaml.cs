﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LoveMusic.Views
{
    public class Artista
    {
        public string Nome { get; set; }
    }

    public partial class ArtistasView : ContentPage
    {
        public List<Artista> Artistas { get; set; }

        public ArtistasView()
        {
            InitializeComponent();

            Artistas = new List<Artista>
            {
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
                new Artista { Nome = "Rag'n'Bone Man" },
            };

            BindingContext = this;
        }

        void ArtistaTappedHandle(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var Artista = (Artista)e.Item;
            Navigation.PushAsync(new MusicasView());
        }
    }
}