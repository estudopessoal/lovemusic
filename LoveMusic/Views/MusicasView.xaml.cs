﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LoveMusic.Views
{
    public class Musica
    {
        public string Nome { get; set; }
        public string Album { get; set; }
        public string Artista { get; set; }
    }

    public partial class MusicasView : ContentPage
    {

        public List<Musica> Musicas { get; set; }

        public MusicasView()
        {
            InitializeComponent();

            Musicas = new List<Musica>
            {
                new Musica {
                    Nome = "Human",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Innocent Man",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Skin",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Bitter End",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Be The Man",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Love You Any Less",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Odetta",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Grace",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Ego",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Arrow",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "As You Are",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Die Easy",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "The Fire",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Fade To Nothing",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Life In Her Yet",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Your Way Or The Rope",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Lay My Body Down",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Wolves",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
                new Musica {
                    Nome = "Healed",
                    Album = "Human",
                    Artista = "Rag'n'Bone Man",
                },
            };

            BindingContext = this;
        }

        void MusicaTappedHandle(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var Musica = (Musica) e.Item;
            DisplayAlert("Notification", string.Format("{0}'s song", Musica.Nome), "Ok");
        }

    }
}
